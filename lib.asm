section .text

global exit
global print_uint
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    mov rsi, [rdi]
    and rsi, 0xff
    test rsi, rsi
    jz .end_loop
    inc rdi
    inc rax
    jmp .loop
.end_loop:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret
; Принимает указатель на нуль-терминированную строку и выводит в stderr
print_error:
   mov rsi, 2
   jmp print_to

; Принимает указатель на нуль-терминированную строку и поток вывода
print_to:
    push rsi
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    pop rdi
    mov rax, 1
    syscall
    ret

; Читает всю строку 
read_line:    
    xor rdx, rdx
    dec rsi
.read_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0xA
    je .line_end
    test al, al
    jz .line_end
    cmp rdx, 256
    ja .too_long
    mov byte [rdi+rdx], al
    inc rdx
    jmp .read_char
.line_end:
    mov byte [rdi+rdx], 0
    mov rax, rdi
    ret
.too_long:
    xor rax, rax
    ret
; Перевод строки
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    sub rsp, 24
    mov byte[rdi], 0
.loop:
    xor rdx, rdx
    div rsi
    or dl, 48
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint
 


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    mov al, byte[rdi]
    mov cl, byte[rsi]
    inc rdi
    inc rsi
    cmp al, cl
    je .is_end_equals
    xor rax, rax
    ret
  .is_end_equals:
    test al, al
    jnz string_equals
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax    ; Проверяем значение rax
    js .end_reading  ; Если значение отрицательное, ret (rax=-1)
    pop rax

.end_reading:
    	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    xor r12, r12
    push rdi
    push rsi

.first_symbol:
    call read_char
    test rax, rax
    js .error_read_char
    cmp al, 10
    je .first_symbol
    cmp al, 9
    je .first_symbol
    cmp al, 0x20
    je .first_symbol
    test al, al
    pop rsi
    pop rdi
    jz .exit_nullterm

.place_symbol:
    cmp r12, rsi
    ja .too_much
    mov byte [rdi + r12], al
    inc r12	
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    cmp al, 10
    je .exit_nullterm
    cmp al, 9
    je .exit_nullterm
    cmp al, 0x20
    je .exit_nullterm
    test al, al
    jz .exit_nullterm
    jmp .place_symbol

.exit_nullterm:
    cmp r12, rsi
    ja .too_much
    mov byte [rdi + r12], 0
    mov rax, rdi
    mov rdx, r12
    pop r12
    ret

.too_much:
    xor rax, rax
    xor rdx, rdx
    pop r12
    ret
.error_read_char:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, 10
    xor r9, r9
.digit_parse_loop:
    mov sil, byte[rdi+r9]
    test sil, sil
    jz .end_loop
    cmp sil, 48
    jb .end_loop
    cmp sil, 57
    ja .end_loop
    sub sil, 48
    inc r9
    mul r8
    add rax, rsi
    jmp .digit_parse_loop
.end_loop:
    mov rdx, r9
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    je .parse_negative
    cmp sil, '+'
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .err
    inc rdx
    ret
.parse_negative:
    inc rdi
    call parse_uint
    test rdx, rdx
    jnz .if_not_error
    ret 
.if_not_error:
    inc rdx
    neg rax
    ret
.err:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
     xor rax, rax
     push rbx
 .copy_loop:
      cmp rax, rdx
      je .buffer_full     
      mov bl, byte[rdi + rax] 
      mov byte[rsi + rax], bl 
      cmp rbx, 0 
      je .end_copy
      inc rax
      jmp .copy_loop
  .buffer_full:
      xor rax, rax
      jmp .end_copy
  .end_copy:
      pop rbx 
      ret


