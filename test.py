import subprocess
inp_str = ["", "third word", "blablabla", "first word", "____________________idsdlfjslifjlisdfjjsdijsdiogjiodjgidsjglidjgflifdjgljdsfligjdslifgjdljfg;lksdjfglkdsjfglkdjsflg;jdslkgjdlskgjldjgfdkfljgkfdjgdkjgkdfjgkdfjgldsfgjdajgfdafgjgdj________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________"]
out_str = ["", "Value found: third word explanation", "",  "Value found: first word explanation", ""]
err_str = ["This entry is not in the list", "", "This entry is not in the list", "", "Line is overflow"]

for i in range(len(inp_str)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=inp_str[i].encode())
    str_out = stdout.decode().strip()
    str_err = stderr.decode().strip()
    if str_out == out_str[i] and str_err == err_str[i]:
        print("Test "+str(i+1)+" passed")
    else:
        print("Test "+str(i+1)+' failed. ./main print {strout: "'+str_out+'", strerr: "'+str_err+'"}, expected: {strout: "'+out_str[i]+'", strerr: "'+err_str[i]+'"}')

